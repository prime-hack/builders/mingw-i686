FROM debian:stable

RUN apt update && apt upgrade -y --force-yes && apt install git upx-ucl cmake binutils-mingw-w64-i686 g++-mingw-w64-i686 mingw-w64-i686-dev mingw-w64-tools -y --force-yes && apt clean

COPY toolchain-win.cmake /usr/share/
COPY i686-w64-mingw32-cmake /usr/bin/

RUN chmod +x /usr/bin/i686-w64-mingw32-cmake
